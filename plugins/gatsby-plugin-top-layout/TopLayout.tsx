import "./../../src/styles/global.scss";

import styled from "@emotion/styled";
import { BoxProps, Box } from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider } from "@mui/material/styles";
import { Container } from "@mui/system";
import * as React from "react";
import { Helmet } from "react-helmet";

import { MorphAnime } from "./../../src/components/morph-anime";
import {
  PaletteModeChoices,
  PaletteModeContext,
  PaletteModeState,
  PalleteModeReducers,
} from "../../src/context/paletteMode";
import { darkTheme, lightTheme } from "../../src/theme";
import { htmlCode } from "./html1"
import { PrismController } from "../../src/components/prism-controller";
interface MainWrapperInterface {
  children: React.ReactNode;
}
interface StyledBoxProps extends BoxProps {
  isLoading: boolean;
}

// markup

const StyledBox = styled.div<StyledBoxProps>`
  opacity: ${(props) => (props.isLoading ? 0 : 1)};
  width: 100%;
  display: flex;
  justify-content: center;
`;

export default function TopLayout({ children }: MainWrapperInterface) {
  // Fuoc Fix
  const [loading, setLoading] = React.useState(true);
  React.useEffect(() => {
    setLoading(false);

    // let btn: any = document.querySelector(".mouse-cursor-gradient-tracking");
    // if (btn) {
    //   btn.addEventListener("mousemove", (e: any) => {
    //     let rect = e.target.getBoundingClientRect();
    //     let x = e.clientX - rect.left;
    //     let y = e.clientY - rect.top;
    //     btn.style.setProperty("--x", x + "px");
    //     btn.style.setProperty("--y", y + "px");
    //   });
    // }

  }, []);
  const [state, dispatch] = React.useReducer(PalleteModeReducers, PaletteModeState);
  const [theme, setTheme] = React.useState(
    state.mode === PaletteModeChoices.Dark ? darkTheme : lightTheme
  );

  React.useEffect(() => {
    setTheme(state.mode === PaletteModeChoices.Dark ? { ...darkTheme } : { ...lightTheme });
  }, [state]);

  return (
    <ThemeProvider theme={theme}>

      <React.Fragment>
      <Box sx={{height: "100%"}}>
  
  <PrismController code={htmlCode} language="html" />
  
        <div id="back-to-top-anchor" />
        <Helmet>
          <meta name="viewport" content="initial-scale=1, width=device-width" />
        </Helmet>

        {/* <MorphAnime /> */}
        <Container maxWidth="xl">
          <CssBaseline />
          <PaletteModeContext.Provider value={{ state, dispatch }}>
            <StyledBox isLoading={loading}>{children}</StyledBox>
          </PaletteModeContext.Provider>
        </Container>
</Box>
      </React.Fragment>
    </ThemeProvider>
  );
}
