/* eslint-disable import/prefer-default-export */
import * as React from "react";
import TopLayout from "./TopLayout";
interface ChildrenInterface {
  element: React.ReactNode;
}

export const wrapRootElement = ({ element }: ChildrenInterface) => {
  return <TopLayout>{element}</TopLayout>;
};
