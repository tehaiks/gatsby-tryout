import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import { Grid } from "@mui/material";
import Box from "@mui/material/Box";
import Fab from "@mui/material/Fab";
import Fade from "@mui/material/Fade";
import useScrollTrigger from "@mui/material/useScrollTrigger";
import * as React from "react";
import { Helmet } from "react-helmet";

// import { CardWelcomeComponent } from "./../components/card-welcome";
import { InfoCards } from "./../components/info-cards";
import { NavBarComponent } from "./../components/nav-bar";
import { TimelineComponent } from "./../components/timeline";
// import { MorphSnap } from "./../components/morph-snap";
import { TypoWelcome } from "./../components/typo-welcome";
interface Props {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window?: () => Window;
  children: React.ReactElement;
}

const IndexPage = (props: Props) => {
  function ScrollTop(props: Props) {
    const { children, window } = props;
    // Note that you normally won't need to set the window ref as useScrollTrigger
    // will default to window.
    // This is only being set here because the demo is in an iframe.
    const trigger = useScrollTrigger({
      target: window ? window() : undefined,
      disableHysteresis: true,
      threshold: 100,
    });

    const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
      const anchor = ((event.target as HTMLDivElement).ownerDocument || document).querySelector(
        "#back-to-top-anchor"
      );

      if (anchor) {
        anchor.scrollIntoView({
          block: "center",
        });
      }
    };

    return (
      <Fade in={trigger}>
        <Box
          onClick={handleClick}
          role="presentation"
          sx={{ position: "fixed", bottom: 16, right: 16 }}
        >
          {children}
        </Box>
      </Fade>
    );
  }

  return (
    <>
      <Helmet>
        <title>~ It is what it is!</title>
      </Helmet>
      <NavBarComponent {...props} />
      {/* <MorphSnap /> */}

      <TypoWelcome />

      <Grid
        container
        spacing={2}
        sx={{
          width: "100%",
          display: "flex",
          flexFlow: "row nowrap",
          alignItems: "center",
        }}
      >
        <Grid item xs={12}>
          {/* <CardWelcomeComponent /> */}
        </Grid>
      </Grid>

      <Grid
        container
        spacing={2}
        sx={{
          width: "100%",
          display: "flex",
          flexFlow: "row nowrap",
          alignItems: "center",
          pb: "300px",
        }}
      >
        <Grid item xs={12}>
          <TimelineComponent />
        </Grid>
      </Grid>
      <Grid sx={{ pb: "300px" }} container>
        <Grid item xs={12}>
          <InfoCards />
        </Grid>
      </Grid>
      <ScrollTop {...props}>
        <Fab size="small" aria-label="scroll back to top">
          <KeyboardArrowUpIcon />
        </Fab>
      </ScrollTop>
    </>
  );
};

export default IndexPage;
