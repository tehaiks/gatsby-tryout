import { createTheme } from '@mui/material/styles';

export const lightTheme = createTheme({
  palette: {
    mode: "light",
    primary: {
      main: "#388E3C",
      // main: "#9bc63f",
    },
    secondary: {
      main: "#099081",
    },
    background: {
      default: "#ffffff",
      paper: "#eeeeee",
    },
  },
  typography: {
    // fontFamily: "Indie Flower",
    fontFamily: "Lato",
    htmlFontSize: 16,
    fontSize: 16,
  },
  spacing: 8,
  shape: {
    borderRadius: 4,
  },
});
export const darkTheme = createTheme({
  palette: {
    mode: "dark",
    primary: {
      main: "#9bc63f",
    },
    secondary: {
      main: "#099081",
    },
    background: {
      default: "#000000",
      paper: "#161616",
    },
  },
  typography: {
    // fontFamily: "Indie Flower",
    fontFamily: "Lato",
    htmlFontSize: 16,
    fontSize: 16,
  },
  spacing: 8,
  shape: {
    borderRadius: 4,
  },
});
