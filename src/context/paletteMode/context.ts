import React from "react";
import {PaletteModeInterface, PaletteModeState} from "./state"

export interface ReducerAction {
  type: string;
  payload: string;
}

export const PaletteModeContext = React.createContext<{
  state: PaletteModeInterface;
  dispatch: React.Dispatch<ReducerAction>;
}>({ state: PaletteModeState, dispatch: () => null });
