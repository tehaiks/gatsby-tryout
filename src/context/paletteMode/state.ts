export enum PaletteModeChoices {
  Light = "light",
  Dark = "dark",
}
import { PaletteMode } from "@mui/material";

export interface PaletteModeInterface {
  mode: PaletteMode
}

export const PaletteModeState: PaletteModeInterface =  {
  mode: PaletteModeChoices.Dark
}