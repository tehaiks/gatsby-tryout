import { PaletteModeInterface } from "./state";
export enum PaletteModeReducersTypes {
  Toggle = "togglePaletteMode"
} 
export const PalleteModeReducers = (
  state: PaletteModeInterface,
  action: { type: string; payload?: any }
): PaletteModeInterface => {
  switch (action.type) {
    case PaletteModeReducersTypes.Toggle:
      return { ...state, mode: action.payload };
  }
  return { ...state };
};
