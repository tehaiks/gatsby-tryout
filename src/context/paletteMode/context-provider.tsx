import React, { useReducer } from "react";
import {
  PalleteModeReducers,
  PaletteModeInterface,
  PaletteModeState,
  PaletteModeContext,
} from "./";

export const RegisterProductProvider = ({ children }: any) => {
  const [state, dispatch] = useReducer(PalleteModeReducers, PaletteModeState);
  return (
    <PaletteModeContext.Provider value={{ state, dispatch }}>
      {children}
    </PaletteModeContext.Provider>
  );
};
