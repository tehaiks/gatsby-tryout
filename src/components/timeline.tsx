import Timeline from "@mui/lab/Timeline";
import TimelineConnector from "@mui/lab/TimelineConnector";
import TimelineContent from "@mui/lab/TimelineContent";
import TimelineDot from "@mui/lab/TimelineDot";
import TimelineItem from "@mui/lab/TimelineItem";
import TimelineSeparator from "@mui/lab/TimelineSeparator";
import * as React from "react";
import {
  SiPostcss,
  SiWebpack,
  SiGatsby,
  SiNuxtdotjs,
  SiReact,
  SiChartdotjs,
  SiTypescript,
  SiGulp,
  SiJavascript,
  SiWordpress,
  SiSass,
  SiLess,
  SiGrunt,
} from "react-icons/si";
import { TbBrandVue, TbBrandReactNative } from "react-icons/tb";

// markup
export const TimelineComponent = () => {
  return (
    <>
      <Timeline position="alternate" sx={{ fontSize: "30px" }}>
        <TimelineItem>
          <TimelineSeparator>
            <TimelineDot variant="outlined" color="grey" />
            <TimelineConnector />
          </TimelineSeparator>
          <TimelineContent
            sx={{
              fontSize: "22px",
              lineHeight: "1.02",
              alignSelf: "self-start",
              fontWeight: "bold",
            }}
          >
            2013
          </TimelineContent>
        </TimelineItem>
        <TimelineItem>
          <TimelineSeparator>
            <TimelineDot variant="outlined" color="secondary" />
            <TimelineConnector />
          </TimelineSeparator>
          <TimelineContent>HTML 4.1, CSS 2</TimelineContent>
        </TimelineItem>
        <TimelineItem>
          <TimelineSeparator>
            <TimelineDot variant="outlined" color="primary" />
            <TimelineConnector />
          </TimelineSeparator>
          <TimelineContent>HTML5, CSS3, jQuery</TimelineContent>
        </TimelineItem>
        <TimelineItem>
          <TimelineSeparator>
            <TimelineDot variant="outlined" color="secondary" />
            <TimelineConnector />
          </TimelineSeparator>
          <TimelineContent>
            <SiGrunt
              style={{
                fill: "transparent",
                stroke: "#D63809",
                width: "20px",
                height: "20px",
                strokeWidth: 0.6,
                strokeLinejoin: "bevel",
                marginRight: "12px",
              }}
            />
            <SiLess
              style={{
                fill: "transparent",
                stroke: "#D63809",
                width: "20px",
                height: "20px",
                strokeWidth: 0.6,
                strokeLinejoin: "bevel",
                marginRight: "12px",
              }}
            />
            <SiSass
              style={{
                fill: "transparent",
                stroke: "#D63809",
                width: "20px",
                height: "20px",
                strokeWidth: 0.6,
                strokeLinejoin: "bevel",
                marginRight: "12px",
              }}
            />
            <br />
            Sass, Less, Grunt.js
          </TimelineContent>
        </TimelineItem>
        <TimelineItem>
          <TimelineSeparator>
            <TimelineDot variant="outlined" color="primary" />
            <TimelineConnector />
          </TimelineSeparator>
          <TimelineContent>
            <SiGulp
              style={{
                fill: "transparent",
                stroke: "#D63809",
                width: "20px",
                height: "20px",
                strokeWidth: 0.6,
                strokeLinejoin: "bevel",
                marginRight: "12px",
              }}
            />
            <SiJavascript
              style={{
                fill: "transparent",
                stroke: "#D63809",
                width: "20px",
                height: "20px",
                strokeWidth: 0.6,
                strokeLinejoin: "bevel",
                marginRight: "12px",
              }}
            />
            <SiWordpress
              style={{
                fill: "transparent",
                stroke: "#D63809",
                width: "20px",
                height: "20px",
                strokeWidth: 0.6,
                strokeLinejoin: "bevel",
                marginRight: "12px",
              }}
            />
            <br />
            Gulp, VanillaJS, Wordpress
          </TimelineContent>
        </TimelineItem>
        <TimelineItem>
          <TimelineSeparator>
            <TimelineDot variant="outlined" color="secondary" />
            <TimelineConnector />
          </TimelineSeparator>
          <TimelineContent>
            <TbBrandVue
              style={{
                fill: "transparent",
                stroke: "#41B883",
                width: "25px",
                height: "25px",
                strokeWidth: 0.6,
                marginRight: "8px",
              }}
            />
            <SiPostcss
              style={{
                fill: "transparent",
                stroke: "#D63809",
                width: "20px",
                height: "20px",
                strokeWidth: 0.6,
                strokeLinejoin: "bevel",
                marginRight: "12px",
              }}
            />
            <SiWebpack
              style={{
                fill: "#8dd6f9",
                stroke: "#fff",
                width: "20px",
                height: "20px",
                strokeWidth: 0,
                marginRight: "8px",
              }}
            />
            <br />
            Vue, PostCSS, Webpack
          </TimelineContent>
        </TimelineItem>
        <TimelineItem>
          <TimelineSeparator>
            <TimelineDot variant="outlined" color="primary" />
            <TimelineConnector />
          </TimelineSeparator>
          <TimelineContent>
            <SiReact
              style={{
                fill: "#8dd6f9",
                stroke: "#fff",
                width: "20px",
                height: "20px",
                strokeWidth: 0,
                marginRight: "8px",
              }}
            />
            <SiChartdotjs
              style={{
                fill: "#8dd6f9",
                stroke: "#fff",
                width: "20px",
                height: "20px",
                strokeWidth: 0,
                marginRight: "8px",
              }}
            />
            <SiTypescript
              style={{
                fill: "#8dd6f9",
                stroke: "#fff",
                width: "20px",
                height: "20px",
                strokeWidth: 0,
                marginRight: "8px",
              }}
            />
            <br />
            React, Chart.js, Typescript
          </TimelineContent>
        </TimelineItem>
        <TimelineItem>
          <TimelineSeparator>
            <TimelineDot variant="outlined" color="secondary" />
            <TimelineConnector />
          </TimelineSeparator>
          <TimelineContent>
            <SiGatsby
              style={{
                fill: "#8dd6f9",
                stroke: "#fff",
                width: "20px",
                height: "20px",
                strokeWidth: 0,
                marginRight: "8px",
              }}
            />
            <SiNuxtdotjs
              style={{
                fill: "#8dd6f9",
                stroke: "#fff",
                width: "20px",
                height: "20px",
                strokeWidth: 0,
                marginRight: "8px",
              }}
            />
            <br />
            Nuxt.js, Gatsby
          </TimelineContent>
        </TimelineItem>
        <TimelineItem>
          <TimelineSeparator>
            <TimelineDot variant="outlined" color="primary" />
            <TimelineConnector />
          </TimelineSeparator>
          <TimelineContent>
            <TbBrandReactNative
              style={{
                fill: "transparent",
                stroke: "#41B883",
                width: "25px",
                height: "25px",
                strokeWidth: 0.6,
                marginRight: "8px",
              }}
            />
            <br />
            React Native, Module Federation
          </TimelineContent>
        </TimelineItem>
        <TimelineItem>
          <TimelineSeparator>
            <TimelineDot variant="filled" color="success" sx={{ width: "20px", height: "20px" }} />
            <TimelineConnector />
          </TimelineSeparator>
          <TimelineContent
            sx={{
              fontSize: "28px",
              lineHeight: "1.1",
              alignSelf: "self-start",
              fontWeight: "bold",
            }}
          >
            2022
          </TimelineContent>
        </TimelineItem>
      </Timeline>
    </>
  );
};
