import styled from "@emotion/styled";
import AppsSharpIcon from "@mui/icons-material/AppsSharp";
import { Box, Card, CardContent, Container, Typography } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import * as React from "react";
// import { gsap } from "gsap-trial";
// import { MorphSVGPlugin } from "gsap-trial/MorphSVGPlugin";
// import { MorphGsap } from "./morph-gsap";
import { MorphAnime } from "./morph-anime";

// gsap.registerPlugin(MorphSVGPlugin);

// markup

export const CardWelcomeComponent = () => {
  const theme = useTheme();

  React.useEffect(() => {
    if (theme.palette.mode === "dark") {
      setBgColors(["#ffffff00", "#ffffff1a", "#ffffff12", "#ffffff3a", "#ffffff1a"]);
    } else {
      setBgColors(["#00000022", "#00000015", "#0000001a", "#0000001a", "#0000001a"]);
    }
  }, [theme]);

  const [bgColors, setBgColors] = React.useState([
    "#ffffff00",
    "#ffffff1a",
    "#ffffff12",
    "#ffffff3a",
    "#ffffff1a",
  ]);

  const StyledCard = styled(Card)`
    background: linear-gradient(-45deg, ${bgColors.map((c) => `${c}`).join(",")});
    background-size: 400% 400%;
    animation: gradient 15s ease-in-out infinite alternate;

    @keyframes gradient {
      0% {
        background-position: 0% 50%;
      }
      50% {
        background-position: 100% 50%;
      }
      100% {
        background-position: 0% 50%;
      }
    }
  `;

  return (
    <Container maxWidth="lg">
      <StyledCard variant="outlined" sx={{ width: "100%" }}>
        <CardContent
          sx={{
            flexFlow: "row nowrap",
            width: "100%",
            alignItems: "center",
            justifyContent: "center",
            display: {
              xs: "none",
              lg: "flex",
            },
          }}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: "100%",
              position: "relative",
            }}
          >
            <AppsSharpIcon sx={{ height: "6em", width: "6em", color: "secondary.main" }} />
            {/* <MorphGsap /> */}
            {/* <MorphAnime /> */}
          </Box>
          <Box
            sx={{
              display: "flex",
              flexFlow: "column wrap",
              alignItems: "flex-start",
              justifyContent: "center",
              p: 3,
            }}
          >
            <Typography
              variant="h5"
              sx={{ mb: 3, fontFamily: "Permanent Marker", fontSize: "2vw", textAlign: "left" }}
            >
              Damian Bursztyka
            </Typography>
            <Typography
              variant="h5"
              sx={{ mb: 3, fontFamily: "Permanent Marker", fontSize: "1.2vw", textAlign: "left" }}
            >
              front-end developer based in Rzeszow, Poland
            </Typography>
            <Typography
              variant="h5"
              sx={{ fontFamily: "Permanent Marker", fontSize: "1.2vw", textAlign: "left" }}
            >
              constant learning + creating front-end solutions from <strong>2013</strong>.
            </Typography>
          </Box>
        </CardContent>

        <CardContent
          sx={{
            flexFlow: "row nowrap",
            width: "100%",
            alignItems: "center",
            justifyContent: "center",
            display: {
              xs: "flex",
              lg: "none",
            },
          }}
        >
          <Box
            sx={{
              display: "flex",
              flexFlow: "column wrap",
              alignItems: "flex-start",
              justifyContent: "center",
              p: 3,
            }}
          >
            <Typography
              variant="h5"
              sx={{
                mb: 3,
                fontFamily: "Lato",
                fontWeight: "bold",
                fontSize: {
                  md: "3.5vw",
                  sm: "3.8vw",
                  xs: "4.2vw",
                },
                textAlign: "left",
              }}
            >
              Damian Bursztyka.
            </Typography>
            <Typography
              variant="h5"
              sx={{
                mb: 3,
                fontFamily: "Lato",
                fontSize: {
                  md: "2.5vw",
                  sm: "2.8vw",
                  xs: "3vw",
                },
                textAlign: "left",
              }}
            >
              front-end developer based in Rzeszów, Poland.
            </Typography>
            <Typography
              variant="h5"
              sx={{
                fontFamily: "Lato",
                fontSize: {
                  md: "2.5vw",
                  sm: "2.6vw",
                  xs: "3vw",
                },
                textAlign: "left",
              }}
            >
              <strong>constant learning</strong> + creating front-end solutions from 2013.
            </Typography>
          </Box>
        </CardContent>
      </StyledCard>
    </Container>
  );
};
