import styled from "@emotion/styled";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import * as React from "react";
import Tilt from "react-parallax-tilt";

// markup

const StyledInfoBox = styled(Box)`
  cursor: default;

  > * {
    border-radius: 5px;
    z-index: 1;
    transition: all 1000ms ease-in-out;
  }

  * {
    user-select: none;
  }

  &:hover {
    transition: all 100ms ease-out;
    z-index: 2;

    > * {
      position: realtive;
      z-index: 2;
      transition: all 100ms ease-in-out;
    }
  }
`;

export const InfoCards = () => {
  return (
    <>
      <Grid container spacing={5} sx={{ px: 6 }}>
        {Array.from(new Array(24)).map((x, i) => (
          <Grid
            item
            xs={12}
            sm={6}
            lg={4}
            xl={3}
            sx={{ position: "relative", mb: 3, p: 3 }}
            key={i}
          >
            <StyledInfoBox className="side side--front">
              <Tilt
                tiltAngleXInitial={1}
                tiltAngleYInitial={1}
                transitionSpeed={333}
                glareEnable={true}
                scale={1.1}
                // gyroscope={true}
                perspective={500}
                tiltReverse={true}
                trackOnWindow={false}
                reset={true}
              >
                <Card color="default">
                  <CardContent className="side--inner">
                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                      CODPEN
                    </Typography>
                    <Typography variant="body2">... asda s asdas da!.</Typography>
                  </CardContent>
                </Card>
              </Tilt>
            </StyledInfoBox>
          </Grid>
        ))}
      </Grid>
    </>
  );
};
