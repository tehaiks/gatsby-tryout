import Brightness4Icon from "@mui/icons-material/Brightness4";
import Brightness7Icon from "@mui/icons-material/Brightness7";
import { Box } from "@mui/material";
import AppBar from "@mui/material/AppBar";
import Container from "@mui/material/Container";
import CssBaseline from "@mui/material/CssBaseline";
import IconButton from "@mui/material/IconButton";
import Slide from "@mui/material/Slide";
import { useTheme } from "@mui/material/styles";
import Toolbar from "@mui/material/Toolbar";
import Tooltip from "@mui/material/Tooltip";
import Typography from "@mui/material/Typography";
import useScrollTrigger from "@mui/material/useScrollTrigger";
import * as React from "react";
import TextIncreaseSharpIcon from "@mui/icons-material/TextIncreaseSharp";
import TextDecreaseSharpIcon from "@mui/icons-material/TextDecreaseSharp";
import {
  PaletteModeChoices,
  PaletteModeContext,
  PaletteModeReducersTypes,
} from "../../src/context/paletteMode";
import BlurOffSharpIcon from "@mui/icons-material/BlurOffSharp";
const pages = ["About me", "Skills", "Resume"];
const settings = ["Profile", "Account", "Dashboard", "Logout"];

interface Props {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window?: () => Window;
  children: React.ReactElement;
}

export const NavBarComponent = (props: Props) => {
  const theme = useTheme();
  const { state, dispatch } = React.useContext(PaletteModeContext);

  const handleFontSizeAdjustment = (direction: string) => {
    console.log(direction);
  };

  const handlePaletteModeChange = () => {
    dispatch({
      type: PaletteModeReducersTypes.Toggle,
      payload:
        state.mode === PaletteModeChoices.Light
          ? PaletteModeChoices.Dark
          : PaletteModeChoices.Light,
    });
  };

  const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(null);
  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(null);

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  function HideOnScroll(props: Props) {
    const { children, window } = props;
    // Note that you normally won't need to set the window ref as useScrollTrigger
    // will default to window.
    // This is only being set here because the demo is in an iframe.
    const trigger = useScrollTrigger({
      target: window ? window() : undefined,
    });

    return (
      <Slide appear={false} direction="down" in={!trigger}>
        {children}
      </Slide>
    );
  }

  return (
    <React.Fragment>
      <CssBaseline />
      {/* <HideOnScroll {...props}> */}
      <AppBar position="fixed" color="default" sx={{ height: "64px" }}>
        <Container>
          <Toolbar disableGutters sx={{ height: "64px" }}>
            {/* <AdbIcon sx={{ display: { xs: "none", md: "flex" }, mr: 1 }} />
              <Typography
                variant="h6"
                noWrap
                component="a"
                href="/"
                sx={{
                  mr: 2,
                  display: { xs: "none", md: "flex" },
                  fontFamily: "monospace",
                  fontWeight: 700,
                  letterSpacing: ".3rem",
                  color: "inherit",
                  textDecoration: "none",
                }}
              >
                LOGO
              </Typography> */}
            <Box
              sx={{
                borderRadius: 1,
                width: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                pr: 4,
              }}
            >
              {/* <Typography variant="body2" sx={{ color: "primary.main" }}>
                change to {theme.palette.mode === "dark" ? "light" : "dark"} mode
              </Typography> */}
              <IconButton sx={{ mx: 2, color: "primary.main" }} onClick={handlePaletteModeChange}>
                <Tooltip placement="left" followCursor={true} title={`Disable Animations`}>
                  <BlurOffSharpIcon />
                </Tooltip>
              </IconButton>
              <IconButton sx={{ mx: 2, color: "primary.main" }} onClick={handlePaletteModeChange}>
                <Tooltip
                  followCursor={true}
                  placement="left"
                  title={`Switch to the ${theme.palette.mode === "dark" ? "light" : "dark"} mode.`}
                >
                  {theme.palette.mode === "dark" ? <Brightness7Icon /> : <Brightness4Icon />}
                </Tooltip>
              </IconButton>
              {/* onClick={handleFontSizeAdjustment("down")} */}
              <IconButton sx={{ mx: 2, color: "primary.dark" }} color="inherit">
                <Tooltip followCursor={true} placement="right" title="Reduce font size.">
                  <TextDecreaseSharpIcon />
                </Tooltip>
              </IconButton>
              {/* onClick={handleFontSizeAdjustment("up")} */}
              <IconButton color="inherit" sx={{ color: "primary.light", mx: 2 }}>
                <Tooltip followCursor={true} placement="right" title="Enlarge the font size.">
                  <TextIncreaseSharpIcon />
                </Tooltip>
              </IconButton>
            </Box>
            {/* <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
                <IconButton
                  size="large"
                  aria-label="account of current user"
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  onClick={handleOpenNavMenu}
                  color="inherit"
                >
                  <MenuIcon />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorElNav}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "left",
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "left",
                  }}
                  open={Boolean(anchorElNav)}
                  onClose={handleCloseNavMenu}
                  sx={{
                    display: { xs: "block", md: "none" },
                  }}
                >
                  {pages.map((page) => (
                    <MenuItem key={page} onClick={handleCloseNavMenu}>
                      <Typography textAlign="center">{page}</Typography>
                    </MenuItem>
                  ))}
                </Menu>
              </Box> */}
            {/* <AdbIcon sx={{ display: { xs: "flex", md: "none" }, mr: 1 }} /> */}
            {/* <Typography
                variant="h5"
                noWrap
                component="a"
                href=""
                sx={{
                  mr: 2,
                  display: { xs: "flex", md: "none" },
                  flexGrow: 1,
                  fontFamily: "monospace",
                  fontWeight: 700,
                  letterSpacing: ".3rem",
                  color: "inherit",
                  textDecoration: "none",
                }}
              >
                LOGO
              </Typography> */}
            {/* <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
              {pages.map((page) => (
                <Button
                  variant="outlined"
                  color="primary"
                  size="small"
                  sx={{ mr: 3, fontWeight: "bold" }}
                  key={page}
                  onClick={handleCloseNavMenu}
                >
                  {page}
                </Button>
              ))}
            </Box> */}

            {/* <Box sx={{ flexGrow: 0 }}>
                <Tooltip title="Open settings">
                  <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                    <Avatar alt="Remy Sharp" src="/static/images/avatar/2.jpg" />
                  </IconButton>
                </Tooltip>
                <Menu
                  sx={{ mt: "45px" }}
                  id="menu-appbar"
                  anchorEl={anchorElUser}
                  anchorOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  open={Boolean(anchorElUser)}
                  onClose={handleCloseUserMenu}
                >
                  {settings.map((setting) => (
                    <MenuItem key={setting} onClick={handleCloseUserMenu}>
                      <Typography textAlign="center">{setting}</Typography>
                    </MenuItem>
                  ))}
                </Menu>
              </Box> */}
          </Toolbar>
        </Container>
      </AppBar>
      {/* </HideOnScroll> */}
    </React.Fragment>
  );
};
