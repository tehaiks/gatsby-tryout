import styled from "@emotion/styled";
import { Box, Typography } from "@mui/material";
import { useTheme, Theme } from "@mui/material/styles";
import * as React from "react";
const StyledBoxContainer = styled(Box)`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  margin-top: 0;
  text-align: right;
  height: 100vh;
`;

const StyledTypo = styled(Typography)`
  text-transform: uppercase;
  padding: 20px;
  background-size: 150% auto;
  color: #fff;
  background-clip: text;
  text-fill-color: transparent;
  animation: textclip 3s linear infinite;
  display: inline-block;
  fontsize: 190px;
`;

export const TypoWelcome = () => {
  const theme = useTheme();

  const [enterText, setEnterText] = React.useState<string | null>(null);

  // ${theme?.palette?.primary?.main} 0%,
  // ${theme?.palette?.primary?.dark} 25%,
  // ${theme?.palette?.secondary?.main} 75%,
  // ${theme?.palette?.secondary?.dark} 100%
  return (
    <StyledBoxContainer>
      <StyledTypo
        className="hero-slogan"
        variant="h3"
        sx={{
          backgroundImage: `linear-gradient(
          -270deg,
          #fff 0%,
          #ffe 25%,
          ${theme?.palette?.primary?.main} 75%,
          #fff 100%

          )`,
        }}
      >
        Hi! My name is <strong>Damian</strong>.<br/> 
        i do <strong>code</strong>.
      </StyledTypo>
    </StyledBoxContainer>
  );
};
