import React, { useEffect } from "react";
import Prism from "prismjs";

import { Box, Card, CardContent, Container, Typography } from "@mui/material";

export const PrismController = ({ code, language }: any) => {
  useEffect(() => {
    Prism.highlightAll();
  }, []);
  return (
    <Box
      className={`mouse-cursor-gradient-tracking`}
      sx={{
        background: "rgba(255,255,255,0.2)",
        position: "absolute",
        top: "0%",
        left: "0%",
        width: "100vw",
        height: "100%",
        overflow: "hidden",
        zIndex: -1,
        fontSize: "12px",
        
      }}
    >
      <pre>
        <code className={`language-${language}`}>
        {Array.from(new Array(12)).map(() => `${code}`)}
          </code>
      </pre>
    </Box>
  );
};
