import styled from "@emotion/styled";
import AppsSharpIcon from "@mui/icons-material/AppsSharp";
import { Box, Card, CardContent, Container, Typography } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import * as React from "react";
import { gsap } from "gsap-trial";
import { MorphSVGPlugin } from "gsap-trial/MorphSVGPlugin";

gsap.registerPlugin(MorphSVGPlugin);

// markup
const StyledSvg = styled.svg`
  color: red;
  fill: transparent;
  stroke: #eee;
  height: 100%;
  width: 100%;
  strokewidth: 0.2;
  display: block;

  path {
    stroke: 0.1;
  }
`;

const StyledPath = styled.path`
  display: none;
`;
const StyledBox = styled(Box)`
  display: none;
`;

export const MorphGsap = () => {
  const theme = useTheme();

  React.useEffect(() => {
    setTimeout(() => {
      var tl = gsap.timeline({ repeat: -1, repeatDelay: 2 });
      tl.to("#js", {
        stagger: {
          //amount: 0.75
          each: 0.0071,
        },
        morphSVG: "#html",
        duration: 0.5,
        type: "rotational",
      });
      tl.to("#js", {
        delay: 1,
        stagger: {
          //amount: 0.75
          each: 0.0071,
        },
        morphSVG: "#js",
        duration: 0.5,
      });
      tl.play();
    }, 2000);
  }, []);

  return (
    <Box>
      <StyledBox>
        <path
          id="html"
          d="M3.5 9H5v6H3.5v-2.5h-2V15H0V9h1.5v2h2V9zm14 0H13c-.55 0-1 .45-1 1v5h1.5v-4.5h1V14H16v-3.51h1V15h1.5v-5c0-.55-.45-1-1-1zM11 9H6v1.5h1.75V15h1.5v-4.5H11V9zm13 6v-1.5h-2.5V9H20v6h4z"
        ></path>
      </StyledBox>
      <StyledSvg id="svg" viewBox="0 0 24 24" strokeWidth="0.1">
        <path
          id="js"
          d="M12 14v-1h1.5v.5h2v-1H13c-.55 0-1-.45-1-1V10c0-.55.45-1 1-1h3c.55 0 1 .45 1 1v1h-1.5v-.5h-2v1H16c.55 0 1 .45 1 1V14c0 .55-.45 1-1 1h-3c-.55 0-1-.45-1-1zM9 9v4.5H7.5v-1H6v1c0 .83.67 1.5 1.5 1.5H9c.83 0 1.5-.67 1.5-1.5V9H9z"
        ></path>
      </StyledSvg>
    </Box>
  );
};
