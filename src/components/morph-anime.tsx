import styled from "@emotion/styled";
import AppsSharpIcon from "@mui/icons-material/AppsSharp";
import { Box, Card, CardContent, Container, Typography } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import * as React from "react";
import Letterize from "letterizejs";
import anime from "animejs";

const unit = "1rem";
const sideLength = 15;
const textAnime = "front-end developer";

// markup
const StyledSvg = styled.svg`
  color: red;
  fill: transparent;
  stroke: #eee;
  height: 100%;
  width: 100%;
  strokewidth: 0.2;
  display: block;

  path {
    stroke: 0.1;
  }
`;

const StyledPath = styled.path`
  display: none;
`;
const StyledBox = styled(Box)`
  // display: grid;
  // grid-template-columns: repeat(${sideLength}, ${unit});
  // grid-template-rows: repeat(${sideLength}, ${unit});
  // align-items: center;
  // grid-gap: 0;
  height: auto;
  width: 100%;
  align-items: center;
  justify-content: center;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-flow: column wrap;
  overflow: hidden;
  .el {
    // width: ${unit};
    // height: ${unit};
    // background: #fff;
    display: inline-block;
    font-size: 1em;
  }

  * {
    transition: 300ms ease-in;
    font-size: 1em;
  }
  .text-main {
    // transition: 1200ms ease-in;
  }
  &.contents-hidden {
    .text,
    .text-main {
      opacity: 0;
    }
  }

  &.animation-started {
    .text-main {
      // font-weight: 600;
    }
  }
`;
const StyledBoxContainer = styled(Box)`
  // display: flex;
  // align-items: center;
  // justify-content: center;
  width: 100%;
  // margin-bottom: 400px;
  margin-top: 200px;
`;

export const MorphAnime = () => {
  const theme = useTheme();

  // };
  const [enterText, setEnterText] = React.useState<string | null>(null);

  // const hydrate = () => {
  //   const elExist = wrapper?.querySelectorAll(".el").length;
  //   if (!elExist) {
  //     const divs = Array.from({ length: Number(`${sideLength}`) }) // lets make an array of 10 divs
  //       .map(function (_, i) {
  //         var el = document.createElement("div");
  //         el.className = "el";
  //         el.textContent = "{textAnime}";
  //         return el;
  //       });
  //     if (wrapper) {
  //       wrapper.append(...divs);
  //     }
  //   }
  // };

  const timeFn = () => {
    setEnterText(textAnime);
    const tm = setTimeout(() => {
      setEnterText("Optimization freak");
      setTimeout(() => {
        setEnterText("Demo expert!");
        setTimeout(() => {
          if (tm) {
            clearTimeout(tm);
          }
          timeFn();
        }, 3400);
      }, 3900);
    }, 3900);
  };
  React.useEffect(() => {
    timeFn();
  }, []);

  React.useEffect(() => {
    // hydrate();

    const wrapper = document.getElementById("box-anime");
    const test = new Letterize({
      targets: ".text:not(.text-main)",
    });

    const tl = anime.timeline({
      targets: test.listAll,
      // loop: true,
      // duration: 0.1,

      // direction: "alternate",
      delay: anime.stagger(88, {
        grid: [test.list[0].length, test.list.length],
        from: "center",
      }),
      easing: "easeInOutQuad",
      begin: () => {
        wrapper?.classList.remove("contents-hidden");
        wrapper?.classList.add("animation-started");
      },
      complete: () => {
        wrapper?.classList.add("contents-hidden");
        wrapper?.classList.remove("animation-started");
      },
    });

    tl.add({
      color: theme.palette.primary.light,
      fontWeight: "300",
      // letterSpacing: "6px",

      endDelay: 0.1,
      duration: 0.2,
    })
      .add({
        letterSpacing: "8px",
        color: theme.palette.secondary.main,
        fontWeight: "300",
        endDelay: 0.5,
        duration: 0.2,
      })
      .add({
        opacity: "0",
        letterSpacing: "8px",
        color: theme.palette.primary.dark,
        fontWeight: "800",
        endDelay: 0.1,
        duration: 2,
      });
    const testmain = new Letterize({
      targets: ".text-main",
    });
    const tlmain = anime.timeline({
      targets: testmain.listAll,
      // loop: true,

      delay: anime.stagger(111, {
        grid: [testmain.list[0].length, testmain.list.length + 1],
        from: "center",
      }),
      easing: "easeInOutQuad",
      // direction: "alternate",
    });

    tlmain
      .add({
        fontWeight: "300",
        // letterSpacing: "4px",
        // fontSize: "1.5em",
        color: theme.palette.secondary.main,
        endDelay: 1,
        duration: 1,
      })
      .add({
        opacity: "1",
        letterSpacing: "8px",
        color: "#fff",
        fontWeight: "800",
        endDelay: 1,
        duration: 1,
      })
      .add({
        opacity: "1",
        duration: 1,
        endDelay: 1,
      })
      .add({
        opacity: "0",
        endDelay: 0,
        duration: 1,
      });
    // .add({
    //   letterSpacing: "8px",
    //   color: theme.palette.secondary.main,
    //   fontWeight: "300",
    //   opacity: "1",
    // })
    // .add({
    //   letterSpacing: "2px",
    //   color: theme.palette.secondary.main,
    //   fontWeight: "900",
    //   opacity: "0",
    // });
    // .add({
    //   letterSpacing: "8px",
    //   color: theme.palette.secondary.main,
    //   fontWeight: "700",
    //   opacity: "1",
    // })
    // .add({
    //   letterSpacing: "4px",
    //   color: theme.palette.secondary.dark,
    //   fontWeight: "100",
    // });
    // tl.add({
    //   targets: "#box-anime .el",
    //   scale: [
    //     { value: 0.1, easing: "easeOutSine", duration: 500 },
    //     { value: 1, easing: "easeInOutQuad", duration: 500 },
    //   ],
    //   delay: anime.stagger(200, { grid: [sideLength, sideLength], from: "center" }),
    //   // translateX: anime.stagger(10, { grid: [14, 5], from: "center", axis: "x" }),
    //   // translateY: anime.stagger(10, { grid: [14, 5], from: "center", axis: "y" }),
    //   // rotateZ: anime.stagger([0, 90], { grid: [14, 5], from: "center", axis: "x" }),
    //   // delay: anime.stagger(200, { grid: [14, 5], from: "center" }),
    //   easing: "easeInOutQuad",
    // });
  }, [enterText]);

  return (
    <StyledBoxContainer>
      <StyledBox id="box-anime" className="contents-hidden">
        <Typography variant="h6" className="text">
          {enterText}
        </Typography>
        <Typography variant="h6" className="text">
          {enterText}
        </Typography>
        <Typography variant="h6" className="text">
          {enterText}
        </Typography>
        <Typography variant="h6" className="text">
          {enterText}
        </Typography>
        <Typography variant="h6" className="text">
          {enterText}
        </Typography>
        <Typography variant="h4" className="text text-main">
          {enterText}
        </Typography>
        <Typography variant="h6" className="text">
          {enterText}
        </Typography>
        <Typography variant="h6" className="text">
          {enterText}
        </Typography>
        <Typography variant="h6" className="text">
          {enterText}
        </Typography>
        <Typography variant="h6" className="text">
          {enterText}
        </Typography>
        <Typography variant="h6" className="text">
          {enterText}
        </Typography>
      </StyledBox>
    </StyledBoxContainer>
  );
};
