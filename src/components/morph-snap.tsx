import styled from "@emotion/styled";
import AppsSharpIcon from "@mui/icons-material/AppsSharp";
import { Box, Card, CardContent, Container, Typography } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import * as React from "react";
import Letterize from "letterizejs";
import anime from "animejs";
import "snapsvg-cjs";
import * as SNAPSVG_TYPE from "snapsvg";
declare var Snap: typeof SNAPSVG_TYPE;

const unit = "1rem";
const sideLength = 15;
const textAnime = "front-end developer";

// markup
const StyledSvg = styled.svg`
  height: 500px;
  width: 500px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding-left: 90px;
  path {
    stroke: #fff;
    stroke-width: 0.2;
    fill: #000;
  }
`;

const StyledBoxContainer = styled(Box)`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;

  // margin-bottom: 400px;
  margin-top: 200px;
`;
export const MorphSnap = () => {
  const theme = useTheme();

  const [enterText, setEnterText] = React.useState<string | null>(null);

  React.useEffect(() => {
    var svg: any = document.getElementById("snap-svg");
    if (svg) {
      var s: any = Snap(svg);
      var svgStart = Snap.select("#svg-start");
      var svgEnd = Snap.select("#svg-end");
      var svgStartPoints = svgStart.node.getAttribute("d");
      var svgEndPoints = svgEnd.node.getAttribute("d");
      var toEnd = function () {
        setTimeout(() => {
          svgStart.animate({ d: svgEndPoints }, 800, mina.easeinout, toStart);
        }, 1200);
      };
      var toStart = function () {
        setTimeout(() => {
          svgStart.animate({ d: svgStartPoints }, 800, mina.easeinout, toEnd);
        }, 1200);
      };
      toStart();
    }
  }, []);

  const svgStartPoints =
    "M14.06 9.94 12 9l2.06-.94L15 6l.94 2.06L18 9l-2.06.94L15 12l-.94-2.06zM4 14l.94-2.06L7 11l-2.06-.94L4 8l-.94 2.06L1 11l2.06.94L4 14zm4.5-5 1.09-2.41L12 5.5 9.59 4.41 8.5 2 7.41 4.41 5 5.5l2.41 1.09L8.5 9zm-4 11.5 6-6.01 4 4L23 8.93l-1.41-1.41-7.09 7.97-4-4L3 19l1.5 1.5z";
  const svgEndPoints =
    "m19 1-5 5v11l5-4.5V1zM1 6v14.65c0 .25.25.5.5.5.1 0 .15-.05.25-.05C3.1 20.45 5.05 20 6.5 20c1.95 0 4.05.4 5.5 1.5V6c-1.45-1.1-3.55-1.5-5.5-1.5S2.45 4.9 1 6zm22 13.5V6c-.6-.45-1.25-.75-2-1v13.5c-1.1-.35-2.3-.5-3.5-.5-1.7 0-4.15.65-5.5 1.5v2c1.35-.85 3.8-1.5 5.5-1.5 1.65 0 3.35.3 4.75 1.05.1.05.15.05.25.05.25 0 .5-.25.5-.5v-1.1z";

  return (
    <StyledBoxContainer>
      <StyledSvg id="snap-svg" viewBox="0 0 31 31" xmlns="http://www.w3.org/2000/svg">
        <path id="svg-start" d={svgStartPoints} fillRule="nonzero" />
        {/* <animate
          dur="5s"
          repeatCount="indefinite"
          attributeName="d"
          values={`${svgStartPoints};${svgEndPoints};${svgStartPoints};`}
          fill="freeze"
          calcMode="spline"
        /> */}
        <path style={{ opacity: 0 }} id="svg-end" d={svgEndPoints} fillRule="nonzero" />
      </StyledSvg>
    </StyledBoxContainer>
  );
};
