import * as React from "react";
import styled from "@emotion/styled";
import { motion } from "framer-motion";
import { useSpring, animated, config, useTrail } from "react-spring";
// styles

const StyledSvg = styled.svg`
  color: red;
  fill: transparent;
  stroke: #000;
  height: 100%;
  width: 100%;
  strokewidth: 1;
  display: block;
`;
// const transition = { duration: 2, yoyo: Infinity, ease: "easeInOut" };

const ellipseVariants = {
  start: {
    d: "M12 9V7h-2V2.84C14.27 3.3 13.42 2 11.99 2 6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12c0-1.05-.17-2.05-.47-3H18zm-2.5-1c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5-1.5-.67-1.5-1.5.67-1.5 1.5-1.5zm-7 0c.83 0 1.5.67 1.5 1.5S9.33 11 8.5 11 7 10.33 7 9.5 7.67 8 8.5 8zm3.5 9.5c-2.33 0-4.31-1.46-5.11-3.5h10.22c-.8 2.04-2.78 3.5-5.11 3.5zM22 3h2v2h-2v2h-2V5h-2V3h2V1h2v2z",
  },
  end: {
    d: "M18 9V7h-2V2.84C14.77 2.3 13.42 2 11.99 2 6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12c0-1.05-.17-2.05-.47-3H18zm-2.5-1c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5-1.5-.67-1.5-1.5.67-1.5 1.5-1.5zm-7 0c.83 0 1.5.67 1.5 1.5S9.33 11 8.5 11 7 10.33 7 9.5 7.67 8 8.5 8zm3.5 9.5c-2.33 0-4.31-1.46-5.11-3.5h10.22c-.8 2.04-2.78 3.5-5.11 3.5zM22 3h2v2h-2v2h-2V5h-2V3h2V1h2v2z",
  },
};

import { Button, Box, Typography } from "@mui/material";

// markup
export const SvgController = () => {
  const [active, setActive] = React.useState(false);
  const { x } = useSpring({ config: { duration: 1500 }, x: active ? 1 : 0 });
  React.useEffect(() => {
    const id = setTimeout(() => {
      setActive(!active);
    }, 2000);

    return () => clearTimeout(id);
  }, [active]);

  React.useEffect(() => {
    setActive(true);
  }, []);

  const paths = [
    {
      id: 1,
      color: "#466FB5",
      d: "M12 14v-1h1.5v.5h2v-1H13c-.55 0-1-.45-1-1V10c0-.55.45-1 1-1h3c.55 0 1 .45 1 1v1h-1.5v-.5h-2v1H16c.55 0 1 .45 1 1V14c0 .55-.45 1-1 1h-3c-.55 0-1-.45-1-1zM9 9v4.5H7.5v-1H6v1c0 .83.67 1.5 1.5 1.5H9c.83 0 1.5-.67 1.5-1.5V9H9z",
    },
    {
      id: 2,
      color: "#0093D3",
      d: "M9.5 14v-1H11v.5h2v-1h-2.5c-.55 0-1-.45-1-1V10c0-.55.45-1 1-1h3c.55 0 1 .45 1 1v1H13v-.5h-2v1h2.5c.55 0 1 .45 1 1V14c0 .55-.45 1-1 1h-3c-.55 0-1-.45-1-1zm7.5 1h3c.55 0 1-.45 1-1v-1.5c0-.55-.45-1-1-1h-2.5v-1h2v.5H21v-1c0-.55-.45-1-1-1h-3c-.55 0-1 .45-1 1v1.5c0 .55.45 1 1 1h2.5v1h-2V13H16v1c0 .55.45 1 1 1zm-9-5c0-.55-.45-1-1-1H4c-.55 0-1 .45-1 1v4c0 .55.45 1 1 1h3c.55 0 1-.45 1-1v-1H6.5v.5h-2v-3h2v.5H8v-1z",
    },
  ];

  const trail = useTrail(paths.length, {
    from: {
      scale: 0,
    },
    to: async (next: any) => {
      while (1) {
        await next({ scale: 1 });
        await next({ scale: 0 });
        await next({ scale: 0 });
      }
    },
  });

  return (
    <>
      <StyledSvg xmlns="http://www.w3.org/2000/svg" viewBox="0, 0, 25, 25" strokeWidth="1">
        {trail.map(({ scale }, index) => {
          const path = paths[index];
          return (
            <animated.path
              key={path.id}
              fill={"transparent"}
              strokeWidth="0.02"
              d={path.d}
              style={{
                transformOrigin: "center",
                transform: scale.interpolate((s: any) => `scale(${s})`),
              }}
            />
          );
        })}

        {/* <motion.path
      d="M24 4c0 .55-.45 1-1 1h-1v1c0 .55-.45 1-1 1s-1-.45-1-1V5h-1c-.55 0-1-.45-1-1s.45-1 1-1h1V2c0-.55.45-1 1-1s1 .45 1 1v1h1c.55 0 1 .45 1 1zm-2.48 4.95c.31.96.48 1.99.48 3.05 0 5.52-4.48 10-10 10S2 17.52 2 12 6.48 2 12 2c1.5 0 2.92.34 4.2.94-.12.33-.2.68-.2 1.06 0 1.35.9 2.5 2.13 2.87C18.5 8.1 19.65 9 21 9c.18 0 .35-.02.52-.05zM7 9.5c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5S9.33 8 8.5 8 7 8.67 7 9.5zm9.31 4.5H7.69c-.38 0-.63.42-.44.75.95 1.64 2.72 2.75 4.75 2.75s3.8-1.11 4.75-2.75c.19-.33-.05-.75-.44-.75zM17 9.5c0-.83-.67-1.5-1.5-1.5S14 8.67 14 9.5s.67 1.5 1.5 1.5 1.5-.67 1.5-1.5z"
      fill="url(#ssvvgg)"
      strokeWidth="1"
      strokeLinecap="round"
      initial={{ pathLength: 0 }}
      animate={{ pathLength: 1 }}
      transition={transition}
    /> */}
        {/* <motion.svg xmlns="http://www.w3.org/2000/svg">
      <motion.path
        strokeWidth={"1"}
        variants={ellipseVariants}
        transition={{
          duration: 2,
          yoyo: Infinity,
          repeat: Infinity,
        }}
      />
    </motion.svg> */}
      </StyledSvg>
      {/* <motion.div
    className="box"
    initial={{ offsetDistance: "0%", scale: 2.5 }}
    animate={{ offsetDistance: "100%", scale: 1 }}
    transition={transition}
  /> */}
    </>
  );
};
