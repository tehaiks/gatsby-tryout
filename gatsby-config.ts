import type { GatsbyConfig } from "gatsby";

const config: GatsbyConfig = {
  siteMetadata: {
    title: `gatsby-tryout`,
    siteUrl: `https://grumpy-net.surge.sh`,
  },
  // More easily incorporate content into your pages through automatic TypeScript type generation and better GraphQL IntelliSense.
  // If you use VSCode you can also use the GraphQL plugin
  // Learn more at: https://gatsby.dev/graphql-typegen
  graphqlTypegen: true,
  plugins: [
    {
      resolve: "gatsby-plugin-bundle-stats",
      options: {
        baseline: true,
        compare: true,
        outDir: "./__performance_data",
        stats: {
          assets: true,
          entrypoints: true,
          chunks: true,
          modules: true,
          builtAt: true,
          hash: true,
        },
      },
    },
    "gatsby-plugin-top-layout",
    "gatsby-plugin-react-helmet",
    {
      resolve: "gatsby-theme-material-ui",
      options: {
        webFontsConfig: {
          fonts: {
            google: [
              {
                family: `Lato`,
                variants: [`300`, `500`, `700`],
              },
              {
                family: `Permanent Marker`,
                variants: [`400`],
              },
            ],
          },
        },
      },
    },
    "gatsby-plugin-sass",
    "gatsby-plugin-emotion",
    "gatsby-plugin-image",
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sitemap",
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        icon: "src/images/icon.png",
      },
    },
    "gatsby-plugin-mdx",
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "./src/images/",
      },
      __key: "images",
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "pages",
        path: "./src/pages/",
      },
      __key: "pages",
    },
  ],
};

export default config;
