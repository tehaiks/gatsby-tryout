exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
  if (stage === "build-html") {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /snap.svg-cjs/,
            use: loaders.null(),
          },
        ],
      },
    })
  }
}