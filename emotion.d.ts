import '@emotion/react';

interface CustomPalette {
}

interface CustomPaletteOptions {
}

interface CustomTheme {
}

declare module "@mui/material/styles" {
  interface Palette extends CustomPalette {}
  interface PaletteOptions extends CustomPaletteOptions {}
  interface Theme extends CustomTheme {}

}

declare module "@emotion/react" {
  export interface Theme extends CustomTheme {}
}
declare module "@emotion/styled" {
  export interface Theme extends CustomTheme {}
}
